Source: node-ip-address
Section: javascript
Priority: optional
Maintainer: Debian Javascript Maintainers <pkg-javascript-devel@lists.alioth.debian.org>
Testsuite: autopkgtest-pkg-nodejs
Build-Depends: debhelper-compat (= 13)
 , dh-sequence-nodejs
 , mocha <!nocheck>
 , node-chai <!nocheck>
 , node-jsbn <!nocheck>
 , node-source-map-support <!nocheck>
 , node-sprintf-js <!nocheck>
 , node-types-jsbn <!nocheck>
 , node-types-sprintf-js <!nocheck>
 , node-typescript
 , ts-node
Standards-Version: 4.6.2
Homepage: https://github.com/beaugunderson/ip-address
Vcs-Git: https://salsa.debian.org/js-team/node-ip-address.git
Vcs-Browser: https://salsa.debian.org/js-team/node-ip-address
Rules-Requires-Root: no

Package: node-ip-address
Architecture: all
Depends: ${misc:Depends}
 , node-jsbn
 , node-sprintf-js
Description: library for parsing IPv4 and IPv6 IP addresses in node and the browser
 ip-address is a library for validating and manipulating IPv4 and IPv6
 addresses in JavaScript.
 .
  * Parsing of all IPv6 notations
  * Parsing of IPv6 addresses and ports from URLs with 'Address6.fromURL(url)'
  * Validity checking
  * Decoding of the Teredo information in an address
  * Whether one address is a valid subnet of another
  * What special properties a given address has (multicast prefix, unique
    local address prefix, etc.)
  * Number of subnets of a certain size in a given address
  * Display methods
    * Hex, binary, and decimal
    * Canonical form
    * Correct form
    * IPv4-compatible (i.e. '::ffff:192.168.0.1')
  * Works in node and the browser (with browserify)
  * ~1,600 test cases
